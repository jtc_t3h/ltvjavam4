package vn.t3h.chapter6;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;

import vn.t3h.dao.CartRepository;

@Controller
public class Bai7_CartsListController {

	CartRepository repository = new CartRepository();
	
	@RequestMapping("chapter6/bai7_carts_index.html")
	public String index(Model model, @CookieValue("cart") String id) {
		model.addAttribute("title", "Your Cart");
		model.addAttribute("list", repository.getCarts(id));
		return "chapter6.bai7.cart.index";
	}
}
