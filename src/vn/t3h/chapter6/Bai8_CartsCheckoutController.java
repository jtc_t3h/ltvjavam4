package vn.t3h.chapter6;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import vn.t3h.dao.CartRepository;
import vn.t3h.dao.InvoiceRepository;
import vn.t3h.domain.Invoice;

@Controller
public class Bai8_CartsCheckoutController {

	private CartRepository repository = new CartRepository();
	private InvoiceRepository invoiceRepository = new InvoiceRepository();

	@RequestMapping("chapter6/bai8_carts_checkout.html")
	public String checkout(Model model, @CookieValue("cart") String id) {
		model.addAttribute("title", "Check Out");
		model.addAttribute("list", repository.getCarts(id));
		return "chapter6.bai8.cart.checkout";
	}

	@RequestMapping(value = "chapter6/bai8_carts_checkout.html", method = RequestMethod.POST)
	public String checkout(Model model, Invoice obj, @CookieValue("cart") String id) {
		obj.setId(id);
		invoiceRepository.add(obj);
		return "redirect:/chapter6/bai9_order_detail.html/" + obj.getId();
	}
}
