package vn.t3h.chapter6;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import vn.t3h.dao.ProductRepository;

@Controller
public class Bai5_ProductSearchController {

	ProductRepository repository = new ProductRepository();

	@RequestMapping("chapter6/bai5-product-search.html")
	public String search(Model model, @RequestParam("q") String q) {
		model.addAttribute("title", "Result for " + q);
		model.addAttribute("list", repository.search(q));
		return "chapter6.bai5.product.search";
	}
}
