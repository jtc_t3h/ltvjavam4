package vn.t3h.chapter6;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import vn.t3h.dao.ProductRepository;
import vn.t3h.domain.Product;

@Controller
public class Bai4_ProductDetailController {

	ProductRepository repository = new ProductRepository();

	@RequestMapping("chapter6/bai4-product-detail.html/{id}")
	public String detail(Model model, @PathVariable("id") int id) {
		Product o = repository.getProduct(id);
//		model.addAttribute("title", o.getTitle());
		model.addAttribute("o", o);
		
		return "chapter6.bai4.product.detail";
	}
}
