package vn.t3h.chapter6;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import vn.t3h.dao.InvoiceRepository;
import vn.t3h.domain.Invoice;

@Controller
public class Bai9_OrderDetailController {

	InvoiceRepository repository = new InvoiceRepository();
	
	@RequestMapping("chapter6/bai10_order_index.html")
	public String index(Model model) {
		List<Invoice> listOfInvoice = repository.getInvoices();
		model.addAttribute("invoices", listOfInvoice);
		return "chapter6.bai10.order.index";
	}
	
	@RequestMapping("chapter6/bai9_order_detail.html/{id}")
	public String detail(Model model, @PathVariable("id") String id) {
		Invoice obj = repository.getInvoice(id);
		model.addAttribute("o", obj);
		model.addAttribute("title", "Order Detail");
		return "chapter6.bai9.order.detail";
	}
}
