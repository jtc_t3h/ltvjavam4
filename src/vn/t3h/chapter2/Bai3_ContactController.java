package vn.t3h.chapter2;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class Bai3_ContactController {

	@RequestMapping("chapter2/bai3-contact.html")
	public String contact() {
		return "chapter2/bai3_contact";
	}

	@RequestMapping(value = "chapter2/bai3-contact.html", method = RequestMethod.POST)
	public String contact(Model model, Bai3_Contact obj) {
		
		model.addAttribute("o", obj);
		return "chapter2/bai3_contact";
	}
}
