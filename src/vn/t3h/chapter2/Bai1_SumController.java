package vn.t3h.chapter2;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class Bai1_SumController {

	// Không định nghĩa thuộc tính method -> mặc định là method GET
	@RequestMapping("chapter2/bai1-sum.html")
	public String index() {
		return "chapter2/bai1_sum";
		
//		definition name = "chapter2_bai1_sum" trong tiles.xml
//		return "chapter2.bai1_sum"; 
	}

	@RequestMapping(value = "chapter2/bai1-sum.html", method = RequestMethod.POST)
	public String index(HttpServletRequest request, int a, int b) {
		
		// Gui du lieu tu Controller -> JSP
		request.setAttribute("a", a);
		request.setAttribute("b", b);
		request.setAttribute("result", a + b);
		
		return "chapter2/bai1_sum";
//		return "chapter2.bai1_sum"; 
	}
}
