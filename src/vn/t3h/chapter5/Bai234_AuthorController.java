package vn.t3h.chapter5;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import vn.t3h.dao.AuthorRepository;
import vn.t3h.domain.Author;

@Controller
@RequestMapping("chapter5/author/")
public class Bai234_AuthorController {

	private AuthorRepository repository = new AuthorRepository();

	@RequestMapping("list.html")
	public String index(Model model) {

		// Lấy danh sách author từ DB
		List<Author> listAuthor = repository.getAuthors();
		// Gửi dữ liệu từ Controller -> trang JSP
		model.addAttribute("list", listAuthor);

		return "chapter5.author.list";
	}

	@RequestMapping("add.html")
	public String add() {
		return "chapter5.author.add";
	}

	@RequestMapping(value = "add.html", method = RequestMethod.POST)
	public String add(Author obj) {
		repository.add(obj);
		return "redirect:/chapter5/author/list.html";
	}
}
