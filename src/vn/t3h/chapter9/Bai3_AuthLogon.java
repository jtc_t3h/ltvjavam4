package vn.t3h.chapter9;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class Bai3_AuthLogon {

	@RequestMapping("/chapter9/bai3-logon.html")
	public String register(HttpServletRequest request) {
		if (request.getParameter("error") != null) {
			request.setAttribute("msg", "Username or password incorrect.");
		}
		return "chapter9.bai3.logon";
	}
}
