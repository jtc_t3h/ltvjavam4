package vn.t3h.chapter9;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import vn.t3h.dao.MemberRepository;
import vn.t3h.domain.Member;

@Controller
public class Bai2_AuthRegisterController {

	MemberRepository repository = new MemberRepository();
	
	@RequestMapping(value = "/chapter9/bai2-register.html", method = RequestMethod.POST)
	public String register(Member obj) {
		repository.add(obj);
		
		return "redirect:/chapter9/bai3-logon.html";
	}
		
	@RequestMapping("/chapter9/bai2-register.html")
	public String register() {
		return "chapter9.bai2.register";
	}
}
