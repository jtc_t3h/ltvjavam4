package vn.t3h.chapter4;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import vn.t3h.domain.EmailInfo;

@Controller
public class Bai3_SendEmailController {

	@Autowired
	private JavaMailSender mailSender;

	@RequestMapping("chapter4/bai3-sendmail.html")
	public String index() {
		return "chapter4.bai3_sendmail";
	}

	@RequestMapping(value = "chapter4/bai3-sendmail.html", method = RequestMethod.POST)
	public String index(Model model, EmailInfo obj) {
		
		SimpleMailMessage message = new SimpleMailMessage();
		message.setTo(obj.getEmail());
		message.setSubject(obj.getSubject());
		message.setText(obj.getSubject());
		try {
			mailSender.send(message);
		} catch (Exception e) {
			model.addAttribute("msg", "send fail.");
			e.printStackTrace();
		}
		
		return "chapter4.bai3_sendmail";
	}
}
