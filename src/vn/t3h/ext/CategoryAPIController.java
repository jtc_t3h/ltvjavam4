package vn.t3h.ext;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CategoryAPIController {

	private CategoryDAO repository = new CategoryDAO();
	
	@GetMapping("/api/category")
	public List<Category> index() {
		List<Category> list = repository.findAll();
		
		list.forEach(e -> e.setParent(repository.findById(e.getParent().getId())));
		
		return list;
	}

	@GetMapping("api/category/{id}")
	public Category detail(@PathVariable("id") int id) {
		return repository.findById(id);
	}

	@PostMapping("api/category")
	public int post(Category obj) {
		return repository.insert(obj);
	}

	@PutMapping("api/category")
	public int edit(Category obj) {
		return repository.update(obj);
	}

	@DeleteMapping("api/category")
	public int delete(int id) {
		return repository.delete(id);
	}
}
