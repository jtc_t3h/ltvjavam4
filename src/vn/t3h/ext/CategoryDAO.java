package vn.t3h.ext;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;


public class CategoryDAO {

	public List<Category> findAll(){
		Session session = HibernateUtils.getSessionFactory().openSession();
		
		Query query = session.createQuery("from Category");
		return query.getResultList();
	}
	
	public Category findById(int id){
		Session session = HibernateUtils.getSessionFactory().openSession();
		
		return session.find(Category.class, id);
	}
	
	public Integer insert(Category category){
		Session session = HibernateUtils.getSessionFactory().openSession();
		return (Integer) session.save(category);
	}
	
	public Integer update(Category category){
		Session session = HibernateUtils.getSessionFactory().openSession();
		try {
			session.update(category);
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	public int delete(Integer id) {
		
		Transaction trans = null;
		try (Session session = HibernateUtils.getSessionFactory().openSession()){
			trans = session.beginTransaction();
			
			Category author = session.find(Category.class, id);
			session.delete(author);
			
			trans.commit();
			return 1;
		} catch (Exception e) {
			if (trans != null) {
				trans.rollback();
			}
			e.printStackTrace();
		}
		return 0;
	}
}
